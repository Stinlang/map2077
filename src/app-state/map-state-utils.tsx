import qs from 'query-string';
import { MAX_ZOOM, MIN_ZOOM } from '../config/map';

interface MinimumViewState {
  longitude: number;
  latitude: number;
  zoom: number;
}

const getValidLatitude = (lat: string) => {
  const latNumber = parseFloat(lat);
  if (!latNumber || latNumber > 90 || latNumber < -90) return null;
  return latNumber;
};

const getValidLongitude = (lng: string) => {
  const lngNumber = parseFloat(lng);
  if (!lngNumber || lngNumber < -180 || lngNumber > -60) return null;
  return lngNumber;
};

const getValidZoom = (zm: string) => {
  const zmNumber = parseFloat(zm);
  if (!zmNumber || zmNumber > MAX_ZOOM || zmNumber < MIN_ZOOM) return null;
  return zmNumber;
};

export const getViewStateFromUrl = (queryString: string) => {
  const { lng, lat, zm } = qs.parse(queryString);
  if (typeof lng !== 'string' || typeof lat !== 'string' || typeof zm !== 'string') {
    return null;
  }

  const longitude = getValidLongitude(lng);
  const latitude = getValidLatitude(lat);
  const zoom = getValidZoom(zm);

  if (!longitude || !latitude || !zoom) {
    return null;
  }

  return { longitude, latitude, zoom, pitch: 0, bearing: 0 };
};

export const getQueryStringFromViewState = ({ longitude, latitude, zoom }: MinimumViewState) => {
  return qs.stringify({
    lng: longitude,
    lat: latitude,
    zm: zoom,
  });
};

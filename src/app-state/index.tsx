import * as React from 'react';
import { layers } from '../components/deck-map/layers';

export type LayerState = { [key: string]: boolean };
type UpdateStateFn<T> = React.Dispatch<React.SetStateAction<T>>;

export interface DeckHoverInfo {
  index: number;
  x: number;
  y: number;
  object: { [key: string]: any } | undefined;
  coordinate: [number, number];
}

const initialLayerState: LayerState = layers
  .map(layer => ({ [layer.id]: true }))
  .reduce((acc, curr) => ({ ...acc, ...curr }), {});

class AppState {
  layerState: LayerState = initialLayerState;
  setLayerState: UpdateStateFn<LayerState> = () => {};

  urlPath: string = '';
  setURLPath: UpdateStateFn<string> = () => {};

  renderLayerMenu: boolean = false;
  setRenderLayerMenu: UpdateStateFn<boolean> = () => {};

  deckHoverInfo: DeckHoverInfo | null = null;
  setDeckHoverInfo: UpdateStateFn<DeckHoverInfo | null> = () => {};
}

export const useAppState = (): AppState => {
  const [layerState, setLayerState] = React.useState(initialLayerState);
  const [urlPath, setPath] = React.useState(window.location.search);
  const [renderLayerMenu, setRenderLayerMenu] = React.useState(false);
  const [deckHoverInfo, setDeckHoverInfo] = React.useState<DeckHoverInfo | null>(null);

  const setURLPath = React.useCallback(
    newPath => {
      setPath(newPath);
      window.history.pushState({}, '', newPath);

      // Trigger popstate event listener
      const popStateEvent = new PopStateEvent('popstate', {});
      dispatchEvent(popStateEvent);
    },
    [setPath],
  );

  return {
    layerState,
    setLayerState,
    urlPath,
    setURLPath,
    renderLayerMenu,
    setRenderLayerMenu,
    deckHoverInfo,
    setDeckHoverInfo,
  };
};

export const AppStateContext = React.createContext<AppState>(new AppState());

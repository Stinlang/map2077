declare module '@deck.gl/core' {
  export class Layer {}
  export class MapController {}
}

class Layer {
  // eslint-disable-next-line
  constructor(...args: any) {}
}

declare module '@deck.gl/layers' {
  export class IconLayer extends Layer {}
  export class BitmapLayer extends Layer {}
}

declare module '@deck.gl/geo-layers' {
  export class TileLayer extends Layer {}
}

declare module '@deck.gl/react' {
  export default class DeckGL extends React.Component<any, any> {}
}

import * as React from 'react';
import styled from 'styled-components';
import LayerMenu from './layer-menu';
import Logo from './logo';
import Footer from './footer';
import { AppStateContext, useAppState } from '../app-state';
import Map from './deck-map';

const BottomContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  position: absolute;
  justify-content: space-between;
  align-items: flex-end;
  bottom: 0;
  z-index: 10;
  pointer-events: none;
`;

const AppRoot = () => {
  return (
    <AppStateContext.Provider value={useAppState()}>
      <Logo />
      <BottomContainer>
        <Footer />
        <LayerMenu />
      </BottomContainer>
      <Map />
    </AppStateContext.Provider>
  );
};

export default AppRoot;

import * as React from 'react';
import styled from 'styled-components';
import { FiLayers } from 'react-icons/fi';
import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { layers } from './deck-map/layers';
import { Theme } from '../config/theme';
import { AppStateContext } from '../app-state';

const LAYER_MANAGER_BUTTON_ID = 'layer-manager';
const LAYER_MANAGER_POPPER = 'layer-popper';

const ButtonContainer = styled.div`
  margin-bottom: 10px;
  margin-right: 10px;
  pointer-events: all;
`;

const LayerEntry = styled.div<{ layerColor: string; visible: boolean }>`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 4px;
  cursor: pointer;
  ${({ visible, layerColor }) => {
    if (!visible) return { filter: 'saturate(0)' };
    return { color: layerColor };
  }}
  color: ${props => props.layerColor};
  &:hover {
    ${({ visible }) => {
      if (visible) {
        return { filter: 'brightness(1.2)' };
      }
      return { filter: 'grayscale(1) brightness(1.3)' };
    }}
  }
`;

const LayerEntryText = styled.div`
  margin: 0 8px;
`;

const LayerEntryIcon = styled.div`
  text-align: center;
  width: 20px;
  height: 18px;
`;

const PopoverContainer = styled.div<{ opacity: number }>`
  padding: 15px 20px;
  height: 250px;
  width: 175px;
  position: absolute;
  bottom: 65px;
  right: 10px;
  pointer-events: ${props => (props.opacity === 1 ? 'all' : 'none')};
  opacity: ${props => props.opacity};
  border: 1px solid ${props => props.theme.primaryThemeColor};
  font-family: ${props => props.theme.fontFamily};
  border-radius: 2px;
  -webkit-box-shadow: 0px 0px 7px 2px rgba(0, 0, 0, 0.5);
  box-shadow: 0px 0px 7px 2px rgba(0, 0, 0, 0.5);
  background-color: rgba(35, 35, 35, 0.95);
  transition: opacity 0.2s linear;
`;

const PrimaryButton = withStyles({
  root: {
    color: Theme.primaryThemeColor,
    border: `1px solid ${Theme.primaryThemeColor}`,
    borderRadius: '2px',
    backgroundColor: 'rgba(35, 35, 35, 0.65)',
    padding: '10px',
    minHeight: 0,
    minWidth: 0,
    '&:hover': {
      border: `1px solid ${Theme.secondaryThemeColor}`,
      backgroundColor: 'rgba(35, 35, 35, 0.45)',
    },
  },
})(Button);

// Dismiss popper if user clicks outside of popper or menu button
const usePopperEffect = () => {
  const { setRenderLayerMenu } = React.useContext(AppStateContext);

  React.useEffect(() => {
    const onMouseDown = (event: any) => {
      const popper = document.getElementById(LAYER_MANAGER_POPPER);
      if (!popper) return;

      const button = document.getElementById(LAYER_MANAGER_BUTTON_ID);
      if (!button) return;

      if ([popper, button].every(e => !e.contains(event.target))) {
        setRenderLayerMenu(false);
      }
    };

    document.addEventListener('mousedown', onMouseDown);
    document.addEventListener('touchstart', onMouseDown);
    return () => {
      document.removeEventListener('mousedown', onMouseDown);
      document.removeEventListener('touchstart', onMouseDown);
    };
  }, [setRenderLayerMenu]);
};

const LayerControlList = () => {
  const { renderLayerMenu, layerState, setLayerState } = React.useContext(AppStateContext);

  usePopperEffect();

  const layerSwitches = React.useMemo(() => {
    return layers.map((layer, index) => {
      const { id, title, renderIcon, color } = layer;
      return (
        <LayerEntry
          layerColor={color}
          visible={layerState[id]}
          key={`${id}-${index}`}
          onClick={() => {
            if (typeof setLayerState === 'function') {
              setLayerState({ ...layerState, [id]: !layerState[id] });
            }
          }}
        >
          <LayerEntryIcon>{renderIcon()}</LayerEntryIcon>
          <LayerEntryText>{title}</LayerEntryText>
        </LayerEntry>
      );
    });
  }, [layerState, setLayerState]);

  return (
    <PopoverContainer id={LAYER_MANAGER_POPPER} opacity={renderLayerMenu ? 1 : 0}>
      {layerSwitches}
    </PopoverContainer>
  );
};

const LayerToggle = () => {
  const { setRenderLayerMenu } = React.useContext(AppStateContext);

  return (
    <ButtonContainer>
      <PrimaryButton
        id={LAYER_MANAGER_BUTTON_ID}
        variant="outlined"
        color="primary"
        onClick={() => setRenderLayerMenu(isOpen => !isOpen)}
      >
        <FiLayers size={20} />
      </PrimaryButton>
      <LayerControlList />
    </ButtonContainer>
  );
};

export default LayerToggle;

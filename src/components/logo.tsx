import * as React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  transform: scale(0.5, 0.5);
  top: -10px;
  left: -150px;
  position: absolute;
  pointer-events: none;
  z-index: 10;
`;

const Logo = () => {
  return (
    <Container>
      <img src="cyberpunk_logo_512.png" alt="Cyberpunk Logo" />
    </Container>
  );
};

export default Logo;

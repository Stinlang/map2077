import * as React from 'react';
import styled from 'styled-components';
import { AppStateContext } from '../../app-state';

const Container = styled.div.attrs<{ left: number; top: number }>(props => ({
  style: {
    left: `${props.left}px`,
    top: `${props.top}px`,
  },
}))<{ left: number; top: number }>`
  position: absolute;
  z-index: 100;
  background-color: rgba(0, 0, 0, 0.65);
  padding: 8px 10px;
  border-radius: 2px;
  color: ${props => props.theme.primaryThemeColor};
  border: 1px solid ${props => props.theme.primaryThemeColor};
`;

const PopperOverlay = () => {
  const { deckHoverInfo } = React.useContext(AppStateContext);
  const { x, y, object } = deckHoverInfo || {};

  if (typeof x !== 'number' || typeof y !== 'number' || typeof object?.label !== 'string') {
    return <div />;
  }

  return (
    <Container left={x + 20} top={y - 16}>
      {object?.label}
    </Container>
  );
};

export default PopperOverlay;

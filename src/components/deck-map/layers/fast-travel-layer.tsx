import { IconLayer } from '@deck.gl/layers';
import Layer, { Type } from './layer';
import data, { PinData } from '../../../data/fast-travel';
import { RenderLayerArgs } from './layer';
import { DeckHoverInfo } from '../../../app-state/index';

const ICON_MAPPING = {
  marker: { x: 0, y: 0, width: 64, height: 64, mask: false, anchorY: 64 },
};

interface DatWithHoverInfo extends PinData {
  size: number;
}

export default class FastTavelLayer extends Layer {
  id = 'fast-travel-layer';
  title = 'Fast Travel';
  type = Type.DECK;
  color = '#3274d9';

  renderIcon() {
    return <img style={{ width: '100%' }} src="icons/fast_travel.png" alt="FT" />;
  }

  renderLayer = ({ visible, onHover, hoverInfo }: RenderLayerArgs) => {
    const dataWithHoverInfo = data.map(pin => {
      const size = pin.label === hoverInfo?.object?.label ? 7 : 5;
      return { ...pin, size };
    });

    return new IconLayer({
      id: this.id,
      data: dataWithHoverInfo,
      pickable: true,
      iconAtlas: 'icons/fast_travel.png',
      iconMapping: ICON_MAPPING,
      getIcon: () => 'marker',
      visible,
      sizeScale: 12,
      getPosition: (d: { coordinates: [number, number] }) => d.coordinates,
      getSize: (obj: DatWithHoverInfo) => obj.size,
      onHover: (info: DeckHoverInfo) => {
        if (typeof onHover === 'function') {
          onHover(info);
        }
      },
    });
  };
}

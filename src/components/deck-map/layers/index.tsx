import FastTavelLayer from './fast-travel-layer';
import MapRoadsLayer from './roads-layer';
import { DeckHoverInfo } from '../../../app-state/index';

interface Options {
  layerToggleState: { [key: string]: boolean };
  hoverInfo: DeckHoverInfo | null;
  onHover: (info: DeckHoverInfo | null) => void;
}

export const layers = [new FastTavelLayer(), new MapRoadsLayer()];

const getDeckLayers = (options?: Options) => {
  const { layerToggleState, onHover, hoverInfo } = options || {};
  const layerIdsToRender = Object.entries(layerToggleState || {})
    .filter(([_, isVisible]) => isVisible)
    .map(([id]) => id);

  return layers.map(layer => {
    const visible = layerIdsToRender.includes(layer.id);
    return layer.renderLayer({ visible, onHover, hoverInfo });
  });
};

export default getDeckLayers;

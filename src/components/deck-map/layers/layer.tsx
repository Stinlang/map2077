import { Layer as DeckLayer } from '@deck.gl/core';
import { Theme } from '../../../config/theme';
import { DeckHoverInfo } from '../../../app-state/index';

export interface RenderLayerArgs {
  visible: boolean;
  hoverInfo?: DeckHoverInfo | null;
  onHover?: (info: DeckHoverInfo | null) => void;
}

export enum Type {
  DECK,
  MAPBOX,
}

export default abstract class Layer {
  abstract id: string;
  abstract title: string;
  abstract type: Type;
  abstract color: string = Theme.textColor;

  abstract renderLayer(args: RenderLayerArgs): DeckLayer | null;
  abstract renderIcon(): JSX.Element;
}

import { FaRoad } from 'react-icons/fa';
import Layer, { Type } from './layer';
import { RenderLayerArgs } from './layer';
export default class MapRoadsLayer extends Layer {
  id = 'roads-layer';
  title = 'Roads';
  type = Type.DECK;
  color = '#9c59ee';

  renderIcon() {
    return <FaRoad />;
  }

  // The roads layer is a mapbox style layer, not a deck.gl layer
  renderLayer = (args: RenderLayerArgs) => null;
}

import * as React from 'react';
import DeckGL from '@deck.gl/react';
import { WebMercatorViewport } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { StaticMap } from 'react-map-gl';
import { AppStateContext } from '../../app-state';
import MapController from './map-controller';
import getDeckLayers from './layers';
import PopoverOverlay from './popper-overlay';
import { getViewStateFromUrl, getQueryStringFromViewState } from '../../app-state/map-state-utils';
import { DEFAULT_VIEW_STATE, MAX_ZOOM } from '../../config/map';

const getMapStyle = (isWebpSupported: boolean, shouldRenderRoads: boolean) => {
  const commonSourceProps = {
    type: 'raster',
    tileSize: 256,
    bounds: [-179.96917991484327, -62.752583746176285, -59.451535366795085, 40.94128301725523],
  };

  const style = {
    version: 8,
    sources: {
      'webp-raster-tiles': {
        ...commonSourceProps,
        tiles: ['https://map2077.s3-us-west-2.amazonaws.com/basemap/{z}/{x}/{y}.webp'],
      },
      'png-raster-tiles': {
        ...commonSourceProps,
        tiles: ['https://map2077.s3-us-west-2.amazonaws.com/basemap-png/{z}/{x}/{y}.png'],
      },
      roads: {
        ...commonSourceProps,
        tiles: ['https://map2077.s3-us-west-2.amazonaws.com/roads/{z}/{x}/{y}.webp'],
      },
    },
    layers: [
      {
        id: 'background',
        type: 'background',
        paint: {
          'background-color': '#000',
        },
      },
      {
        id: 'basemap-tiles',
        type: 'raster',
        source: isWebpSupported ? 'webp-raster-tiles' : 'png-raster-tiles',
        minzoom: 0,
        maxzoom: MAX_ZOOM + 1,
      },
    ],
  };

  if (shouldRenderRoads) {
    style.layers.push({
      id: 'road-tiles',
      type: 'raster',
      source: shouldRenderRoads ? 'roads' : '',
      minzoom: 0,
      maxzoom: MAX_ZOOM + 1,
    });
  }

  return style;
};

const Map = () => {
  const { layerState, setURLPath, deckHoverInfo, setDeckHoverInfo } = React.useContext(
    AppStateContext,
  );

  // Set url to default view state if querystring is empty or invalid
  React.useEffect(() => {
    if (!getViewStateFromUrl(window.location.search)) {
      setURLPath(`?${getQueryStringFromViewState(DEFAULT_VIEW_STATE)}`);
    }
  }, [setURLPath]);

  const [worldCopyIndex, setWorldCopyIndex] = React.useState(0);
  const [viewState, setViewState] = React.useState(
    getViewStateFromUrl(window.location.search) || DEFAULT_VIEW_STATE,
  );

  const layers = React.useMemo(() => {
    return getDeckLayers({
      layerToggleState: layerState,
      hoverInfo: deckHoverInfo,
      onHover: info => {
        setDeckHoverInfo(info?.object ? info : null);
      },
    });
  }, [layerState, setDeckHoverInfo, deckHoverInfo]);

  const wheelPoll = React.useRef<any>();

  const isWebpSupported = React.useMemo(() => {
    const canvas = document.createElement('canvas');
    if (!!(canvas.getContext && canvas.getContext('2d'))) {
      return canvas.toDataURL('image/webp').indexOf('data:image/webp') === 0;
    }

    return false;
  }, []);

  const mapboxStyle = React.useMemo(() => {
    return getMapStyle(isWebpSupported, layerState['roads-layer']);
  }, [isWebpSupported, layerState]);

  // Prevent horizontal wrapping in order to adapt to map with no world copies
  const updateWorldCopyIndex = React.useCallback((current, old) => {
    if (Math.abs(old.longitude - current.longitude) > 300) {
      setWorldCopyIndex(index => (old.longitude < current.longitude ? index - 1 : index + 1));
    }
  }, []);

  return (
    <>
      <DeckGL
        mapStyle={mapboxStyle}
        controller={{ type: MapController, dragRotate: false, touchRotate: false }}
        viewState={{ ...viewState, longitude: viewState.longitude + 360 * worldCopyIndex }}
        onClick={({ lngLat }: { lngLat: [number, number] }) => console.log(lngLat)}
        layers={layers}
        onViewStateChange={({
          viewState,
          oldViewState,
          interactionDetails,
        }: {
          viewState: any;
          oldViewState: WebMercatorViewport;
          interactionDetails: any;
        }) => {
          const { isZooming, panEnd } = interactionDetails || {};

          // Update url lat lng only when pan ends
          if (panEnd) {
            setURLPath(`?${getQueryStringFromViewState(viewState)}`);
          }

          // Update url zm only after 200 ms of not zooming to prevent excessive updates
          if (isZooming) {
            clearTimeout(wheelPoll.current);
            wheelPoll.current = setTimeout(() => {
              if (isZooming) {
                setURLPath(`?${getQueryStringFromViewState(viewState)}`);
              }
            }, 200);
          } else {
            // Remove popovers when panning
            setDeckHoverInfo(null);
            updateWorldCopyIndex(viewState, oldViewState);
          }
          setViewState(viewState);
        }}
      >
        {/* @ts-ignore */}
        <StaticMap mapStyle={mapboxStyle} mapOptions={{ renderWorldCopies: false }} />
      </DeckGL>
      <PopoverOverlay />
      <div style={{ backgroundColor: '#000', height: '100vh' }} />
    </>
  );
};

export default Map;

// @ts-nocheck - deck.gl does not have typescript types yet
import { MapController } from '@deck.gl/core';
import { MIN_ZOOM, MAX_ZOOM } from '../../config/map';

const NO_TRANSITION_PROPS = {
  transitionDuration: 0,
};

const ZOOM_ACCEL = 0.01;

export default class NoWrapMapController extends MapController {
  updateViewport(newControllerState, extraProps = {}, interactionState = {}, interactionDetails) {
    const viewState = Object.assign({}, newControllerState.getViewportProps(), extraProps);
    const changed = this.controllerState !== newControllerState;

    if (changed) {
      const oldViewState = this.controllerState ? this.controllerState.getViewportProps() : null;
      if (this.onViewStateChange) {
        this.onViewStateChange({ viewState, interactionState, oldViewState, interactionDetails });
      }
    }

    Object.assign(this._state, newControllerState.getInteractiveState(), interactionState);
    if (this.onStateChange) this.onStateChange(this._state);
  }

  _project(pos) {
    const viewport = this.makeViewport(this.controllerState._viewportProps);
    return pos && viewport.project(pos);
  }

  _onPanEnd() {
    const newControllerState = this.controllerState[this._panMove ? 'panEnd' : 'rotateEnd']();
    this.updateViewport(
      newControllerState,
      null,
      { isDragging: false, isPanning: false, isRotating: false },
      { panEnd: true },
    );
    return true;
  }

  _onPanMove(event) {
    if (!this.dragPan) return false;

    const pos = this.getCenter(event);
    const newControllerState = this.controllerState.pan({ pos });

    this.updateViewport(
      newControllerState,
      NO_TRANSITION_PROPS,
      { isDragging: true, isPanning: true },
      {},
    );

    return true;
  }

  zoom({ pos, startPos, scale }) {
    let { startZoom, startZoomLngLat } = this.controllerState._interactiveState;

    if (!Number.isFinite(startZoom)) {
      startZoom = this.controllerState._viewportProps.zoom;
      startZoomLngLat =
        this.controllerState._unproject(startPos) || this.controllerState._unproject(pos);
    }

    // Clamp final zoom between min and max zoom levels
    const zoom = Math.max(
      Math.min(this.controllerState._calculateNewZoom({ scale, startZoom }), MAX_ZOOM),
      MIN_ZOOM,
    );

    const zoomedViewport = this.controllerState.makeViewport({
      ...this.controllerState._viewportProps,
      zoom,
    });
    const [longitude, latitude] = zoomedViewport.getMapCenterByLngLatPosition({
      lngLat: startZoomLngLat,
      pos,
    });

    return this.controllerState._getUpdatedState({
      zoom,
      longitude,
      latitude,
    });
  }

  _onWheel(event) {
    super._onWheel(event);
    if (!this.scrollZoom) {
      return false;
    }
    event.preventDefault();

    const pos = this.getCenter(event);
    if (!this.isPointInBounds(pos, event)) {
      return false;
    }

    const { delta } = event;

    // Map wheel delta to relative scale
    let scale = 2 / (1 + Math.exp(-Math.abs(delta * ZOOM_ACCEL)));
    if (delta < 0 && scale !== 0) {
      scale = 1 / scale;
    }

    const newControllerState = this.zoom({ pos, scale });
    this.updateViewport(
      newControllerState,
      NO_TRANSITION_PROPS,
      { isZooming: true, isPanning: true },
      { isZooming: true },
    );

    return true;
  }

  _onPinch(event) {
    if (!this.touchZoom && !this.touchRotate) {
      return false;
    }
    if (!this.isDragging()) {
      return false;
    }

    let newControllerState = this.controllerState;
    if (this.touchZoom) {
      const { scale } = event;
      const pos = this.getCenter(event);
      newControllerState = this.zoom({ pos, scale });
    }
    if (this.touchRotate) {
      const { rotation } = event;
      const { startPinchRotation } = this._state;
      newControllerState = newControllerState.rotate({
        deltaScaleX: -(rotation - startPinchRotation) / 180,
      });
    }

    this.updateViewport(newControllerState, NO_TRANSITION_PROPS, {
      isDragging: true,
      isPanning: this.touchZoom,
      isZooming: this.touchZoom,
      isRotating: this.touchRotate,
    });
    return true;
  }
}

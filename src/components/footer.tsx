import * as React from 'react';
import styled from 'styled-components';
import { FiInfo } from 'react-icons/fi';
import { Theme } from '../config/theme';

const DISPLAY_DURATION_MS = 7000;

interface ExternalLinkProps {
  href: string;
  text: string;
}

const FooterContainer = styled.div<{ opacity?: number }>`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  z-index: 10;
  color: #555;
  padding: 8px;
  pointer-events: ${props => (props.opacity === 0 ? 'none' : 'all')};
`;

const FooterText = styled.div<{ opacity?: number }>`
  color: ${Theme.textColor};
  font-size: 12px;
  margin: 0 6px;
  padding: 3px 4px;
  background-color: rgba(25, 25, 25, 0.7);
  user-select: none;
  transition: opacity 1s;
  opacity: ${props => props.opacity};
`;

const Link = styled.a`
  color: ${Theme.linkColor};
  text-decoration: underline;
`;

const ExternalLink = ({ href, text }: ExternalLinkProps) => (
  <Link target="_blank" rel="noopener noreferrer" href={href}>
    {text}
  </Link>
);

const Footer = () => {
  const [opacity, setOpacity] = React.useState(1);
  const previousTimeout = React.useRef<null | NodeJS.Timeout>(null);

  // Hide text after display duration has elapsed
  const hideText = React.useCallback(() => {
    previousTimeout.current = setTimeout(() => {
      setOpacity(0);
    }, DISPLAY_DURATION_MS);
  }, []);

  React.useEffect(() => {
    hideText();
    return () => {
      if (previousTimeout.current) clearTimeout(previousTimeout.current);
    };
  }, [hideText]);

  return (
    <FooterContainer opacity={opacity}>
      <div style={{ position: 'relative', top: '2px' }}>
        <FiInfo
          size={18}
          style={{
            cursor: 'pointer',
            pointerEvents: 'all',
            color: Theme.primaryThemeColor,
          }}
          onClick={() => setOpacity(1)}
          onMouseLeave={hideText}
        />
      </div>
      <FooterText
        opacity={opacity}
        onMouseEnter={() => {
          // Prevent text from disappearing if user hovers over it
          if (previousTimeout.current) clearTimeout(previousTimeout.current);
          setOpacity(1);
        }}
        onMouseLeave={hideText}
      >
        Created by <ExternalLink href="https://github.com/austintang" text="Austin Tang" /> as a
        fan-made proof-of-concept project. Cyberpunk 2077 assets including but not limited to logo
        and satellite imagery are property of{' '}
        <ExternalLink href="https://en.cdprojektred.com/" text="CD Projekt Red" />.
      </FooterText>
    </FooterContainer>
  );
};

export default Footer;

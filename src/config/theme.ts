export const Theme = {
  primaryThemeColor: '#fcec51',
  secondaryThemeColor: '#e3dc94',
  tertiaryThemeColor: '#b5b188',

  textColor: '#cccccc',
  linkColor: '#367fd1',
  fontFamily: `'Syne', -apple-system, BlinkMacSystemFont, 'Segoe UI', Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif`,
};

export const DEFAULT_VIEW_STATE = {
  latitude: 0.542801848514038,
  longitude: -128.4360755480144,
  zoom: 4.293518066658515,
  pitch: 0,
  bearing: 0,
};

export const MIN_ZOOM = 2;
export const MAX_ZOOM = 7;

export interface PinData {
  label: string;
  coordinates: [number, number];
}

const data: Array<PinData> = [
  {
    label: '101 North',
    coordinates: [-144.40062913874382, 34.068892547345385],
  },
  {
    label: 'Oil Fields',
    coordinates: [-136.8639856018031, 35.418100058886196],
  },
  {
    label: 'Offshore St',
    coordinates: [-130.37615860248232, 27.558788871035695],
  },
  {
    label: 'Ebunike Docks',
    coordinates: [-132.8978094875389, 28.020413070828223],
  },
  {
    label: 'Longshore North',
    coordinates: [-122.88658239276859, 24.270476944145358],
  },
  {
    label: 'Docks',
    coordinates: [-137.28126748040523, 25.386626382131958],
  },
  {
    label: 'Arasaka Waterfront North',
    coordinates: [-141.565936484428, 26.344023086835904],
  },
  {
    label: 'Pershing St',
    coordinates: [-135.730587992514, 21.34629987255845],
  },
  {
    label: 'Martin St',
    coordinates: [-132.17977377267226, 20.44964501840505],
  },
  {
    label: 'All Foods Plant',
    coordinates: [-124.16312901558217, 19.981901385244676],
  },
  {
    label: 'Kabuki Market',
    coordinates: [-129.22210549457728, 18.194143377187267],
  },
  {
    label: 'Kennedy North',
    coordinates: [-129.8043808485639, 16.7378446196363],
  },
  {
    label: 'Megabuilding H10',
    coordinates: [-131.73248131317976, 17.713497536067674],
  },
  {
    label: 'Metro: Med Center',
    coordinates: [-132.0510848087573, 15.318005233784369],
  },
  {
    label: 'Charter St',
    coordinates: [-125.68123414260151, 16.235420705919278],
  },
  {
    label: 'East',
    coordinates: [-122.25349998328446, 16.509416631494016],
  },
  {
    label: 'Drive-In Theater',
    coordinates: [-116.87668019038043, 17.895443251794134],
  },
  {
    label: 'Drake Ave',
    coordinates: [-134.33156648349652, 15.568433196132451],
  },
  {
    label: 'Metro: Eisenhower St',
    coordinates: [-136.17726949235967, 16.539512910622786],
  },
  {
    label: 'California & Pershing',
    coordinates: [-140.54011533039923, 17.903133569020714],
  },
  {
    label: 'California & Cartwright',
    coordinates: [-139.05148514769945, 14.173138974041716],
  },
  {
    label: 'Sutter St',
    coordinates: [-129.73667522845605, 13.850515901731836],
  },
  {
    label: 'Creek Loop',
    coordinates: [-128.05576713109843, 13.338070125155195],
  },
  {
    label: 'Bellevue Overwalk',
    coordinates: [-128.91270067092788, 12.427967077331159],
  },
  {
    label: 'Goldsmith St',
    coordinates: [-133.36216328157974, 13.145620126859292],
  },
  {
    label: 'Metro: Farrier St',
    coordinates: [-134.32896009574614, 12.352871938936216],
  },
  {
    label: 'Clarendon St',
    coordinates: [-138.46581111636053, 9.034876137801275],
  },
  {
    label: 'Kabuki: Central',
    coordinates: [-129.52331306632362, 11.055885302659485],
  },
  {
    label: 'Allen St South',
    coordinates: [-128.40270766808527, 10.473204868808022],
  },
  {
    label: 'Pinewood St South',
    coordinates: [-129.20470957074613, 8.778521524885685],
  },
  {
    label: 'Megabuilding H10: Atrium',
    coordinates: [-131.9457982654564, 10.084137944400938],
  },
  {
    label: 'Metro: Zocalo',
    coordinates: [-132.58300525661159, 8.957586194861568],
  },
  {
    label: 'Bradbury & Buran',
    coordinates: [-133.73656963715112, 9.705427282833156],
  },
  {
    label: 'Afterlife',
    coordinates: [-132.7720937752111, 7.688001147603858],
  },
  {
    label: 'Metro: Ellison St',
    coordinates: [-131.8267038862689, 7.305769755867564],
  },
  {
    label: 'Riot',
    coordinates: [-134.8698557206653, 6.917454272683264],
  },
  {
    label: 'Japantown West',
    coordinates: [-126.14482144638454, 10.506683361974778],
  },
  {
    label: 'Sagan & Diamond',
    coordinates: [-124.23829567484538, 10.090624364857389],
  },
  {
    label: 'Capitola St',
    coordinates: [-121.93068523090739, 13.749255906820352],
  },
  {
    label: 'Skyline & Salinas',
    coordinates: [-121.23986313128086, 11.449852405195173],
  },
  {
    label: 'Cherry Blossom Market',
    coordinates: [-124.0635362920205, 6.614025886378691],
  },
  {
    label: 'Megabuilding H8',
    coordinates: [-124.04705336027776, 4.301663058778098],
  },
  {
    label: 'Metro: Monroe St',
    coordinates: [-122.11966437607434, 8.284429598429213],
  },
  {
    label: 'Fourth Wall Studios',
    coordinates: [-119.48411672448671, 7.880972462221291],
  },
  {
    label: 'Redwood Market',
    coordinates: [-120.4524420700136, 3.606494844446793],
  },
  {
    label: 'Crescent & Broad',
    coordinates: [-120.80407794718512, 1.1429604029090588],
  },
  {
    label: 'Gold Niwaki Plaza',
    coordinates: [-118.63255734379864, -0.9047704629832065],
  },
  {
    label: 'Dark Matter',
    coordinates: [-119.50065841556552, -1.887508473292964],
  },
  {
    label: 'Metro: Charter Hill',
    coordinates: [-117.56116678054144, -2.853236300775385],
  },
  {
    label: 'Lele Park',
    coordinates: [-118.08862059629868, -5.339970569626168],
  },
  {
    label: 'Luxury Apartments',
    coordinates: [-116.72603157225919, -4.962563805361168],
  },
  {
    label: 'Longshore South',
    coordinates: [-116.02050277907752, -8.319330348243158],
  },
  {
    label: 'Dynalar',
    coordinates: [-113.85939196945309, -7.575807817812498],
  },
  {
    label: 'Silk Road West',
    coordinates: [-123.95173893591168, 0.162263878357458],
  },
  {
    label: 'Metro: Japantown South',
    coordinates: [-122.64378130898032, -2.2563509879173],
  },
  {
    label: 'Republic East',
    coordinates: [-114.67788103487197, -11.539285492055592],
  },
  {
    label: 'San Amaro St',
    coordinates: [-108.35346143799725, -11.051257919950565],
  },
  {
    label: 'Trailer Park',
    coordinates: [-102.52543730004034, -11.026996025954515],
  },
  {
    label: 'Berkeley & Bruce Skiv',
    coordinates: [-137.21037674517208, 3.2857059209360844],
  },
  {
    label: 'Downtown North',
    coordinates: [-133.35337071744755, 3.4885494577457727],
  },
  {
    label: 'Metro: Republic Way',
    coordinates: [-132.82591690169036, 0.9204795411237645],
  },
  {
    label: 'Metro: Downtown - Alexander St',
    coordinates: [-139.53445660939028, 1.0903631582161009],
  },
  {
    label: 'Skyline & Republic',
    coordinates: [-142.83554556275678, 0.5294121656441104],
  },
  {
    label: 'Downtown Central',
    coordinates: [-140.06962807765703, -1.4401702258158557],
  },
  {
    label: 'Gold Beach Marina',
    coordinates: [-143.27830545684677, -3.4318111534134452],
  },
  {
    label: 'Corporation St',
    coordinates: [-143.32225994149314, -5.2060473779906795],
  },
  {
    label: 'Halsey & MLK',
    coordinates: [-135.99159410121277, -2.286571864668892],
  },
  {
    label: 'Arasaka Tower',
    coordinates: [-132.49721257182117, -3.904251083821828],
  },
  {
    label: 'Metro: Memorial Park',
    coordinates: [-131.14561216894327, -4.829669765173603],
  },
  {
    label: 'Ring Road',
    coordinates: [-129.14568311753035, -2.873601993464437],
  },
  {
    label: 'Petrel St',
    coordinates: [-126.57581072586862, -3.2421846651903397],
  },
  {
    label: 'Delamain HQ',
    coordinates: [-126.19808098400986, -5.324379302305882],
  },
  {
    label: 'Republic & Vine',
    coordinates: [-122.83705715849142, -5.776860585001978],
  },
  {
    label: 'Skyline East',
    coordinates: [-123.50001139930467, -9.312957398617831],
  },
  {
    label: 'Metro: Congress & MLK',
    coordinates: [-127.78166156190007, -8.514677234897563],
  },
  {
    label: 'College St',
    coordinates: [-127.21025326149652, -11.004163307972322],
  },
  {
    label: 'Congress & Madison',
    coordinates: [-129.93543130957553, -9.458518793568013],
  },
  {
    label: 'Reconciliation Park',
    coordinates: [-134.51768633396637, -8.156044335671258],
  },
  {
    label: 'Berkeley & Bay',
    coordinates: [-141.4295290446181, -8.55812534671344],
  },
  {
    label: 'Cannery Plaza',
    coordinates: [-143.77926081110536, -10.43214534819417],
  },
  {
    label: 'Metro: Market St',
    coordinates: [-140.10788767158814, -12.190758147280457],
  },
  {
    label: 'Senate & Market',
    coordinates: [-137.54365305247993, -10.825950897584379],
  },
  {
    label: 'Embers',
    coordinates: [-136.2138142549772, -10.169329995381492],
  },
  {
    label: 'Metro: Glen North',
    coordinates: [-133.72593401658682, -11.630682331931288],
  },
  {
    label: 'Metro: Ebunike',
    coordinates: [-131.59437422154653, -11.755243013163362],
  },
  {
    label: 'Palms View Plaza',
    coordinates: [-142.84670705062365, -13.449970240990334],
  },
  {
    label: 'Pumping Station',
    coordinates: [-143.44015070705296, -15.142228513780385],
  },
  {
    label: 'Parque Del Mar',
    coordinates: [-142.21479945349944, -16.731681467843227],
  },
  {
    label: 'Megabuilding H2',
    coordinates: [-140.12126210998437, -16.868375345934677],
  },
  {
    label: 'Palms View Way',
    coordinates: [-135.5924487232982, -14.291316949716785],
  },
  {
    label: 'Ventura & Skyline',
    coordinates: [-137.22991362714995, -17.800477579369247],
  },
  {
    label: 'Valentino Alley',
    coordinates: [-134.35610777240353, -17.54931258614182],
  },
  {
    label: 'Metro: Glen South',
    coordinates: [-134.09784988488326, -18.85354464683217],
  },
  {
    label: 'El Coyote Cojo',
    coordinates: [-131.16120014505194, -15.58916230486659],
  },
  {
    label: 'Hanford Overpass',
    coordinates: [-129.92096207908912, -17.852925843939126],
  },
  {
    label: 'Megabuilding H3',
    coordinates: [-129.42545882243047, -14.636468353219536],
  },
  {
    label: 'Shooting Range',
    coordinates: [-127.99166216486462, -14.60588140242625],
  },
  {
    label: 'Pacifica Pier',
    coordinates: [-137.41102975139728, -22.275292094533537],
  },
  {
    label: "Batty's Hotel",
    coordinates: [-137.22969974526586, -25.503433441465216],
  },
  {
    label: 'Chapel',
    coordinates: [-136.20765789252627, -25.612431838344957],
  },
  {
    label: 'Metro: Stadium',
    coordinates: [-133.59210992530015, -25.632239067550728],
  },
  {
    label: 'Stadium Parking',
    coordinates: [-130.8776546820024, -22.716724982506694],
  },
  {
    label: 'Grand Imperial Mall',
    coordinates: [-140.9190540850664, -28.24878248124485],
  },
  {
    label: 'West Wind Apartments',
    coordinates: [-146.24482274976663, -31.02929003286777],
  },
  {
    label: 'Abandoned Parking Lot',
    coordinates: [-124.8638196374136, -30.49105866745596],
  },
  {
    label: 'Solar Power Station',
    coordinates: [-118.06021163108402, -32.23688387975488],
  },
  {
    label: 'Dam',
    coordinates: [-106.11104267040548, -35.779478914113085],
  },
  {
    label: 'Lake Farm',
    coordinates: [-105.69343417143625, -40.30527830112879],
  },
  {
    label: 'Las Palapas Motel',
    coordinates: [-131.49757844137878, -42.03596744391298],
  },
  {
    label: 'Solar Arrays',
    coordinates: [-123.57893983071875, -43.202491307688426],
  },
  {
    label: 'Fuel Station',
    coordinates: [-136.49849815553438, -47.093662319036255],
  },
  {
    label: 'Autowerks',
    coordinates: [-117.02988757522427, -47.38277067874176],
  },
  {
    label: 'Tango Tors Motel',
    coordinates: [-118.38162034820235, -51.46611647656195],
  },
  {
    label: 'Regional Airport',
    coordinates: [-133.29777087482674, -52.61478464492119],
  },
  {
    label: 'Abandoned Fuel Station',
    coordinates: [-128.79219554303737, -55.96867287373896],
  },
  {
    label: 'Border Checkpoint',
    coordinates: [-147.98574909590096, -54.999417058907426],
  },
  {
    label: 'Protein Farm',
    coordinates: [-155.593235881798, -45.98462112263379],
  },
  {
    label: 'Megabuilding H4',
    coordinates: [-127.04259401990159, -23.82053596964836],
  },
  {
    label: 'Metro: Wolleson St',
    coordinates: [-128.16354314871268, -20.206384015701335],
  },
  {
    label: 'Rancho Coronado South',
    coordinates: [-121.70715233749763, -24.638403310157415],
  },
  {
    label: 'Piez',
    coordinates: [-117.8202192277014, -26.29919256386044],
  },
  {
    label: 'Almunecar & Jerez',
    coordinates: [-115.41897035863033, -25.379851926033542],
  },
  {
    label: 'Rancho Coronado East',
    coordinates: [-111.16022313779548, -27.70143459436504],
  },
  {
    label: 'Tama Viewpoint',
    coordinates: [-107.31382906834548, -27.26782692280172],
  },
  {
    label: 'Hargreaves St',
    coordinates: [-123.82177211537542, -18.296806633436827],
  },
  {
    label: 'Arasaka Industrial Park',
    coordinates: [-118.20054192530758, -20.878379365466955],
  },
  {
    label: 'Kendal Park',
    coordinates: [-111.94566139451977, -22.92473715681136],
  },
  {
    label: 'Mallagra & Manzanita',
    coordinates: [-112.54459989961964, -20.09967567501993],
  },
  {
    label: 'Red Dirt Bar',
    coordinates: [-124.44315849945203, -15.33228868843091],
  },
  {
    label: 'MLK & Brandon',
    coordinates: [-121.82761053222569, -12.882435990363659],
  },
  {
    label: 'Megabuilding H6',
    coordinates: [-118.45926829712143, -13.934823369611507],
  },
  {
    label: 'Metro: Megabuilding H7',
    coordinates: [-114.69492571217326, -17.255450430889024],
  },
  {
    label: 'Rancho Coronado North',
    coordinates: [-108.1174576089774, -15.324780655151438],
  },
  {
    label: 'Sunset Motel',
    coordinates: [-97.5249931064563, -12.654589698680429],
  },
  {
    label: 'Medeski Fuel Station',
    coordinates: [-99.68318786514173, -19.60818565252163],
  },
  {
    label: 'Nomad Camp',
    coordinates: [-95.56187598310166, 20.714188012270686],
  },
  {
    label: 'Desert Film Set',
    coordinates: [-79.74212675251816, 2.612533361663983],
  },
  {
    label: 'Rocky Ridge',
    coordinates: [-87.31888093166522, -5.457291841588297],
  },
  {
    label: 'Edgewood Farm',
    coordinates: [-88.17042276159106, -18.540638350590886],
  },
  {
    label: 'Mobile Camp',
    coordinates: [-77.18837169217076, -8.447047366109997],
  },
  {
    label: 'Sunshine Motel',
    coordinates: [-75.39292646944907, -14.410989209077693],
  },
  {
    label: 'Old Turbines',
    coordinates: [-74.0636822374035, -22.899905503796028],
  },
  {
    label: 'Wraith Camp',
    coordinates: [-81.12242234390294, -28.402732061144437],
  },
  {
    label: 'Big Rock',
    coordinates: [-66.73475587578423, -12.535080090641642],
  },
  {
    label: 'I-9 East',
    coordinates: [-59.73134952868385, -12.711310457872079],
  },
  {
    label: 'Far Ridge',
    coordinates: [-64.54238537086661, -27.53120675698171],
  },
  {
    label: 'Arasaka Estate',
    coordinates: [-110.56787142123927, 9.472833372215762],
  },
  {
    label: "Kerry Eurodyne's Residence",
    coordinates: [-113.55742261869054, 7.871312710361231],
  },
  {
    label: 'North Oaks Sign',
    coordinates: [-112.90895379277283, 4.700142148643492],
  },
  {
    label: 'Columbarium',
    coordinates: [-114.34877440625125, 2.167668623673525],
  },
];

export default data;

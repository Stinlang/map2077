# Map 2077

An Interactive Map for the world of Cyberpunk 2077

## Generating Map Tiles from an image with gdal2tiles

In order to get full vertical coverage of the map, it is absolutely important to have a source image that has a width and height that is a power of two (preferably 8192).

Use the following steps to generate map tiles from a high-resolution source image:

- Pull in the latest `osgeo/gdal` docker container with the command `docker pull osgeo/gdal`

- Use the following command to generate tiles with gdal:

  ```bash
  docker run --rm -v <absolute_path_to_source_file>:/home osgeo/gdal gdal2tiles.py -p raster -r bilinear -z 2-6 -w none --xyz /home/<source_file_name> /home/<map_tile_output_dir_name>
  ```

  An example would look like the following:

  ```bash
  docker run --rm -v /Users/username/Desktop/:/home osgeo/gdal gdal2tiles.py -p raster -r bilinear -z 2-6 -w none --xyz /home/source_image.png /home/raster-tiles
  ```
